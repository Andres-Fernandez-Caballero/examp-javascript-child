const botonTodos = document.getElementById("boton-todos");
const botonAgregar = document.getElementById("boton-agregar");
const botonLimpiar = document.getElementById("boton-limpiar");

const listaUsuarios = [
  { nombre: "andres", rol: "admin" },
  { nombre: "pepe", rol: "user" },
  { nombre: "juan", rol: "admin" },
  { nombre: "martin", rol: "user" },
];

function agregarTodos() {
  console.log("estoy soy yo ", this);
  this.disabled = true;
  const contenedor = document.getElementById("contenedor");

  const contenedorLista = document.createElement("ul");
  contenedorLista.id = "contenedor-lista";

  listaUsuarios.forEach((usuario) => {
    const itemList = document.createElement("li");
    itemList.innerHTML = `<p>Usuario: ${usuario.nombre} <span>ROL: ${usuario.rol}</span></p>`;
    contenedorLista.appendChild(itemList);
  });

  contenedor.appendChild(contenedorLista);
}

function agregarUnoMas() {
  const contenedorLista = document.getElementById("contenedor-lista");
  if (contenedorLista) {
    const otroUsuario = {
      nombre: "otro usuario",
      rol: "user",
    };
    const itemList = document.createElement("li");
    itemList.innerHTML = `<p>Usuario: ${otroUsuario.nombre} <span>ROL: ${otroUsuario.rol}</span></p>`;
    contenedorLista.appendChild(itemList);
  }
}

function limpiarTodo() {
  const contenedor = document.getElementById("contenedor");
  while (contenedor.firstChild) {
    const child = contenedor.firstChild;
    contenedor.removeChild(child);
  }
  botonAgregar.disabled = false;
  botonTodos.disabled = false;
}

botonTodos.addEventListener("click", agregarTodos);
botonAgregar.addEventListener("click", agregarUnoMas);
botonLimpiar.addEventListener("click", limpiarTodo);
